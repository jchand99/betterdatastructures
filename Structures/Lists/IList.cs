using System;
using System.Collections.Generic;

namespace Structures.Lists
{
    public interface IList<T> : IEnumerable<T>
    {
        int Count { get; }

        void Add(T element);

        void Add(int position, T element);

        T Remove(int position);

        void Empty();

        T Replace(int position, T element);

        T[] ToArray();

        bool Contains(T element);

        bool IsEmpty();

        void Sort();
    }
}