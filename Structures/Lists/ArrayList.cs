﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Structures.Lists
{
    public class ArrayList<T> : IList<T>, IEnumerable<T> where T : IEquatable<T>, IComparable
    {
        private T[] List;
        private const int DefaultCapacity = 10;
        private bool Initialized = false;

        public int Count { get; private set; }
        public int Capacity { get; private set; }

        public ArrayList() : this(DefaultCapacity) { }

        public ArrayList(int capacity)
        {
            List = new T[capacity + 1];
            Capacity = capacity;
            Count = 0;
            Initialized = true;
        }

        public ArrayList(T[] array)
        {
            Capacity = array.Length;
            Count = 0;
            List = new T[Capacity];
            Initialized = true;

            for (int i = 0; i < array.Length; i++)
            {
                Add(array[i]);
            }
        }

        private void CheckIsInitialized()
        {
            if (!Initialized)
            {
                throw new AccessViolationException($"Cannot access uninitialized ArrayList.");
            }
        }

        public T this [int position]
        {
            get
            {
                CheckIsInitialized();
                if (position >= 1 && position <= Count)
                {
                    return List[position];
                }
                else
                {
                    throw new InvalidOperationException($"Position {position} is invalid.");
                }
            }
        }

        public void Add(T element)
        {
            CheckIsInitialized();
            Add(Count + 1, element);
        }

        public void Add(int position, T element)
        {
            CheckIsInitialized();

            if (position >= 1 && position < List.Length)
            {
                List[position] = element;
                Count++;
            }
            else
            {
                Resize();
                Add(position, element);
            }
        }

        private void Resize()
        {
            T[] temp = new T[List.Length * 2];
            for (int i = 1; i < List.Length; i++)
            {
                temp[i] = List[i];
            }

            List = temp;
            Capacity = List.Length - 1;
        }

        public bool Contains(T element)
        {
            CheckIsInitialized();

            for (int i = 1; i <= Count; i++)
            {
                if (element.Equals(List[i]))
                {
                    return true;
                }
            }
            return false;
        }

        public void Empty()
        {
            CheckIsInitialized();

            List = new T[Count];
            Count = 0;
            Capacity = List.Length;
        }

        public IEnumerator<T> GetEnumerator()
        {
            CheckIsInitialized();

            TrimExcess();
            return new ArrayListEnumerator(this);
        }

        public bool IsEmpty()
        {
            CheckIsInitialized();

            return Count.Equals(0);
        }

        public T Remove(int position)
        {
            CheckIsInitialized();

            T removed = List[position];

            for (int i = position; i <= Count; i++)
            {
                List[i] = List[i + 1];
            }

            Count--;
            return removed;
        }

        public T Replace(int position, T element)
        {
            CheckIsInitialized();

            T replaced = List[position];
            List[position] = element;

            return replaced;
        }

        public T[] ToArray()
        {
            CheckIsInitialized();

            T[] arr = new T[Count];
            for (int i = 1; i <= Count; i++)
            {
                arr[i - 1] = List[i];
            }
            return arr;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator) GetEnumerator();
        }

        public void TrimExcess()
        {
            T[] arr = new T[Count + 1];
            for (int i = 1; i <= Count; i++)
            {
                arr[i] = List[i];
            }

            List = arr;
            Capacity = List.Length - 1;
        }

        public void Sort()
        {
            QuickSort(1, Count);
        }

        private void QuickSort(int beginning, int end)
        {
            if (beginning < end)
            {
                int pivot = Partition(beginning, end);
                QuickSort(beginning, pivot - 1);
                QuickSort(pivot + 1, end);
            }
        }

        private int Partition(int beginning, int end)
        {
            T pivot = List[end];
            int index = beginning - 1;

            for (int j = beginning; j <= end - 1; j++)
            {
                if (List[j].CompareTo(pivot) <= 0)
                {
                    index++;
                    Swap(index, j);
                }
            }
            Swap(index + 1, end);
            return index + 1;
        }

        private void Swap(int index, int j)
        {
            T temp = List[index];
            List[index] = List[j];
            List[j] = temp;
        }

        internal class ArrayListEnumerator : IEnumerator<T>
        {
            private int Position = 0;
            private ArrayList<T> List;

            public T Current
            {
                get
                {
                    try
                    {
                        return List[Position];
                    }
                    catch (IndexOutOfRangeException)
                    {
                        throw new InvalidOperationException($"Position {Position} is invalid.");
                    }
                }
            }

            object IEnumerator.Current => Current;

            public ArrayListEnumerator(ArrayList<T> list)
            {
                List = list;
            }

            public bool MoveNext()
            {
                Position++;
                return Position <= List.Count;
            }

            public void Reset()
            {
                Position = 0;
            }

            #region IDisposable Support
            private bool disposedValue = false; // To detect redundant calls

            protected virtual void Dispose(bool disposing)
            {
                if (!disposedValue)
                {
                    if (disposing)
                    {
                        // TODO: dispose managed state (managed objects).
                    }

                    // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                    // TODO: set large fields to null.

                    disposedValue = true;
                }
            }

            // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
            // ~ArrayListEnumerator() {
            //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            //   Dispose(false);
            // }

            // This code added to correctly implement the disposable pattern.
            public void Dispose()
            {
                // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
                Dispose(true);
                // TODO: uncomment the following line if the finalizer is overridden above.
                // GC.SuppressFinalize(this);
            }
            #endregion
        }
    }
}