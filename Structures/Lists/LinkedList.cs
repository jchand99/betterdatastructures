using System;
using System.Collections;
using System.Collections.Generic;

namespace Structures.Lists
{
    public class LinkedList<T> : IList<T> where T : IEquatable<T>, IComparable
    {
        private Node RootNode;
        private bool Initialized = false;

        public int Count { get; private set; }

        public LinkedList()
        {
            Initialized = true;
            Count = 0;
        }

        public LinkedList(T[] array)
        {
            // TODO: Implement me
            throw new NotImplementedException();
        }

        private void CheckInitialized()
        {
            if (!Initialized)
            {
                throw new InvalidOperationException($"Cannot access uninitialized ArrayList.");
            }
        }

        public T this [int position]
        {
            get
            {
                CheckInitialized();
                if (position >= 1 && position <= Count)
                {
                    return GetNodeAt(position).Element;
                }
                else
                {
                    throw new InvalidOperationException($"Position {position} is invalid.");
                }
            }
        }

        private Node GetNodeAt(int index)
        {
            Node currentNode = RootNode;

            for (int i = 1; i < index; i++)
            {
                currentNode = currentNode.NextNode;
            }

            return currentNode;
        }

        public void Add(T element)
        {
            CheckInitialized();

            Node node = new Node(element);

            if (IsEmpty())
            {
                RootNode = node;
            }
            else
            {
                Node end = GetNodeAt(Count);
                end.NextNode = node;
            }

            Count++;
        }

        public void Add(int position, T element)
        {
            CheckInitialized();

            Node node = new Node(element);

            if (position >= 1 && position <= Count)
            {
                if (position == 1)
                {
                    RootNode = node;
                }
                else
                {
                    Node before = GetNodeAt(position - 1);
                    Node after = GetNodeAt(position);
                    before.NextNode = node;
                    node.NextNode = after;
                }
            }

            Count++;
        }

        public bool Contains(T element)
        {
            CheckInitialized();

            Node currentNode = RootNode;

            for (int i = 1; i < Count; i++)
            {
                if (currentNode.Element.Equals(element))
                {
                    return true;
                }
                currentNode = currentNode.NextNode;
            }

            return false;
        }

        public void Empty()
        {
            CheckInitialized();

            RootNode = null;
            Count = 0;
        }

        public IEnumerator<T> GetEnumerator()
        {
            CheckInitialized();

            return new LinkedListEnumerator(this);
        }

        public bool IsEmpty()
        {
            return Count == 0;
        }

        public T Remove(int position)
        {
            CheckInitialized();

            Node before = GetNodeAt(position - 1);
            Node after = GetNodeAt(position + 1);
            Node current = before.NextNode;

            before.NextNode = after;

            Count--;
            return current.Element;
        }

        public T Replace(int position, T element)
        {
            CheckInitialized();

            Node current = GetNodeAt(position);

            T item = current.Element;
            current.Element = element;

            return item;
        }

        public void Sort()
        {
            // TODO: Implement me
            throw new NotImplementedException();
        }

        public T[] ToArray()
        {
            Node currentNode = RootNode;

            T[] array = new T[Count];
            for (int i = 1; i <= Count; i++)
            {
                array[i - 1] = currentNode.Element;
                currentNode = currentNode.NextNode;
            }

            return array;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return (IEnumerator) GetEnumerator();
        }

        internal class Node
        {
            internal T Element { get; set; }
            internal Node NextNode { get; set; }

            internal Node() : this(default(T), null) { }

            internal Node(T element) : this(element, null) { }

            internal Node(T element, Node next)
            {
                Element = element;
                NextNode = next;
            }
        }

        internal class LinkedListEnumerator : IEnumerator<T>
        {
            private int Position = 0;
            private LinkedList<T> List;

            public T Current
            {
                get
                {
                    try
                    {
                        return List[Position];
                    }
                    catch (IndexOutOfRangeException)
                    {
                        throw new InvalidOperationException($"Position {Position} is invalid.");
                    }
                }
            }

            object IEnumerator.Current => Current;

            public LinkedListEnumerator(LinkedList<T> list)
            {
                List = list;
            }

            public bool MoveNext()
            {
                Position++;
                return Position <= List.Count;
            }

            public void Reset()
            {
                Position = 0;
            }

            #region IDisposable Support
            private bool disposedValue = false; // To detect redundant calls

            protected virtual void Dispose(bool disposing)
            {
                if (!disposedValue)
                {
                    if (disposing)
                    {
                        // TODO: dispose managed state (managed objects).
                        List = null;
                    }

                    // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                    // TODO: set large fields to null.

                    disposedValue = true;
                }
            }

            // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
            // ~LinkedListEnumerator() {
            //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            //   Dispose(false);
            // }

            // This code added to correctly implement the disposable pattern.
            public void Dispose()
            {
                // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
                Dispose(true);
                // TODO: uncomment the following line if the finalizer is overridden above.
                // GC.SuppressFinalize(this);
            }
            #endregion
        }
    }
}