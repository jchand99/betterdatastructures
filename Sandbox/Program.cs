﻿using System;
using Structures.Lists;

namespace Sandbox
{
    class Program
    {
        static void Main(string[] args)
        {
            IList<int> list = new LinkedList<int>();
            list.Add(1);
            list.Add(3);
            list.Add(4);
            list.Add(5);
            list.Add(7);
            list.Add(8);

            list.Remove(3);

            int[] arr = list.ToArray();

            foreach(int i in arr)
            {
                Console.WriteLine(i);
            }
        }
    }
}
