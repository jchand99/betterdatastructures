using NUnit.Framework;
using Structures.Lists;

namespace Tests.Lists
{
    public class LinkedListTests
    {

        private LinkedList<int> List;
        private int[] arr;

        [SetUp]
        public void Setup()
        {
            List = new LinkedList<int>();
            List.Add(1);
            List.Add(3);
            List.Add(200);
            List.Add(12);
            List.Add(40);
            List.Add(30);
            List.Add(0);

            arr = new int[] { 0, 1, 3, 12, 30, 40, 200 };
        }

        [Test]
        public void CountTest()
        {
            Assert.AreEqual(7, List.Count);
        }

        [Test]
        public void BracketIndexTest()
        {
            Assert.AreEqual(200, List[3]);
        }

        [Test]
        public void ForEachTest()
        {
            int[] arr = new int[List.Count];
            int index = 0;
            foreach(int i in List)
            {
                arr[index] = i;
                index++;
            }

            Assert.AreEqual(List.ToArray(), arr);
        }

        [Test]
        public void SortTest()
        {
            List.Sort();
            Assert.AreEqual(arr, List.ToArray());
        }

        [Test]
        public void RemoveTest()
        {
            arr = new int[] { 1, 3, 12, 40, 30, 0 };
            List.Remove(3);
            Assert.AreEqual(arr, List.ToArray());
        }

        [Test]
        public void ContainsTest()
        {
            Assert.True(List.Contains(200));
        }

        [Test]
        public void DoesNotContainTest()
        {
            Assert.False(List.Contains(1000));
        }

        [Test]
        public void IsNotEmptyTest()
        {
            Assert.False(List.IsEmpty());
        }

        [Test]
        public void EmptyTest()
        {
            List.Empty();
            Assert.AreEqual(0, List.Count);
        }

        [Test]
        public void IsEmptyTest()
        {
            List.Empty();
            Assert.True(List.IsEmpty());
        }
    }
}