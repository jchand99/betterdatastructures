using NUnit.Framework;
using Structures.Lists;

namespace Tests.Lists
{
    public class ArrayListTests
    {

        private ArrayList<int> List;
        private int[] Arr;
        private int[] CopyArray;

        [SetUp]
        public void Setup()
        {
            List = new ArrayList<int>(20);
            List.Add(1);
            List.Add(3);
            List.Add(200);
            List.Add(12);
            List.Add(40);
            List.Add(30);
            List.Add(0);

            CopyArray = new int[] { 1, 3, 200, 12, 40, 30, 0 };


            Arr = new int[] { 0, 1, 3, 12, 30, 40, 200 };
        }

        [Test]
        public void CapacityTest()
        {
            Assert.AreEqual(20, List.Capacity);
        }

        [Test]
        public void BracketIndexTest()
        {
            Assert.AreEqual(200, List[3]);
        }

        [Test]
        public void ForEachTest()
        {
            bool isIncorrect = false;

            int index = 0;
            foreach(int i in List)
            {
                if(i != CopyArray[index])
                {
                    isIncorrect = true;
                }
                index++;
            }

            Assert.IsFalse(isIncorrect);
        }

        [Test]
        public void SortTest()
        {
            List.Sort();
            List.TrimExcess();
            Assert.AreEqual(Arr, List.ToArray());
        }

        [Test]
        public void TrimExcessTest()
        {
            List.TrimExcess();
            Assert.AreEqual(7, List.Capacity);
        }

        [Test]
        public void RemoveTest()
        {
            Arr = new int[] { 1, 3, 12, 40, 30, 0 };
            List.Remove(3);
            List.TrimExcess();
            Assert.AreEqual(Arr, List.ToArray());
        }

        [Test]
        public void ContainsTest()
        {
            Assert.True(List.Contains(200));
        }

        [Test]
        public void DoesNotContainTest()
        {
            Assert.False(List.Contains(1000));
        }

        [Test]
        public void IsNotEmptyTest()
        {
            Assert.False(List.IsEmpty());
        }

        [Test]
        public void EmptyTest()
        {
            List.Empty();
            Assert.AreEqual(0, List.Count);
        }

        [Test]
        public void IsEmptyTest()
        {
            List.Empty();
            Assert.True(List.IsEmpty());
        }
    }
}